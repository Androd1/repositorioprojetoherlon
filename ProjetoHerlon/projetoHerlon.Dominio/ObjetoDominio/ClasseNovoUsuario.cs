﻿using System;
using System.Collections.Generic;

namespace projetoHerlon.Dominio.ObjetoDominio
{
    public class ClasseNovoUsuario
    {
        public DadosPessoais _DadosPessoais { get; set; }
        public string _EstadoCivil { get; set; }
        public Conjugue _Conjugue { get; set; }
        public DadosSistema _DadosSistema { get; set; }
        public DadosCadastro _DadosCadastro { get; set; }
        public Endereço _Endereço { get; set; }
        public Contatos _Contatos { get; set; }
        public DadosVendedor _DadosVendedor { get; set; }
        public DadosConsumo _DadosConsumo { get; set; }
        public List<Clientes> _Clientes { get; set; }

    }
    public class DadosPessoais
    {
        public string Nome { get; set; }
        public string CpfCnpj { get; set; }
        public string RG { get; set; }
        public string OrgãoEmissor { get; set; }
        public string FiliaçãoMãe { get; set; }
        public string FiliaçãoPai { get; set; }
        public string Sexo { get; set; }
        public string Nascimento { get; set; }
    }
    public class Conjugue
    {
        public DadosPessoais DadosPessoaisConjugue { get; set; }
    }
    public class DadosSistema
    {
        public int NivelAcesso { get; set; }
        public string TokenTemp { get; set; }
        public string Observações { get; set; }
        public string AreaDeMensagens { get; set; }
    }
    public class DadosCadastro
    {
        public DateTime DataDeCadastro { get; set; }
        public string QuemCadastrou { get; set; }
        public string Chave { get; set; }
        public string Região { get; set; }
    }
    public class Endereço
    {
        public string Rua { get; set; }
        public int Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string Pais { get; set; }
        public double CEP { get; set; }
    }
    public class Contatos
    {
        public string Celular1 { get; set; }
        public string Celular2 { get; set; }
        public string Residencial { get; set; }
        public string Comercial { get; set; }
        public string Email { get; set; }
        public string Site { get; set; }

    }
    public class DadosVendedor
    {
        public string Status { get; set; }
        public double DescontoMaximoAplicavel { get; set; }
        public List<Vendas> VendasConcretizadas { get; set; }
        public List<Vendas> VendasRealizadas { get; set; }
    }
    public class DadosConsumo
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double ConsumoMedio { get; set; }
    }
    public class Vendas
    {
        public int Ano { get; set; }
        public double Jan { get; set; }
        public double Fev { get; set; }
        public double Mar { get; set; }
        public double Abr { get; set; }
        public double Mai { get; set; }
        public double Jun { get; set; }
        public double Jul { get; set; }
        public double Ago { get; set; }
        public double Set { get; set; }
        public double Out { get; set; }
        public double Nov { get; set; }
        public double Dez { get; set; }
    }
    public class Clientes
    {
        public ClasseNovoUsuario Cliente { get; set; }
    }
}