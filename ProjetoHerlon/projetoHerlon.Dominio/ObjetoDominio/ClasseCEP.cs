﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetoHerlon.Dominio.ObjetoDominio
{
    public class ClasseCEP
    {
        public string CEP { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
    }
}