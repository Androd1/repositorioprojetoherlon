﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetoHerlon.Dominio.ObjetoDominio
{
    public class ClasseObjetoCalculadora
    {
        public bool Excel { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Consumo { get; set; }
        public int Celula { get; set; }
        public double Eficiencia { get; set; }
    }
}