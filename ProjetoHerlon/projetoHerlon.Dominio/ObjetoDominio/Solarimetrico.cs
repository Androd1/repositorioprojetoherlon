﻿namespace projetoHerlon.Dominio.ObjetoDominio
{
    public class Solarimetrico
    {
        public double Id { get; set; }
        public string Country { get; set; }
        public double Lon { get; set; }
        public double Lat { get; set; }
        public double Annual { get; set; }
        public double Jan { get; set; }
        public double Feb { get; set; }
        public double Mar { get; set; }
        public double Apr { get; set; }
        public double May { get; set; }
        public double Jun { get; set; }
        public double Jul { get; set; }
        public double Aug { get; set; }
        public double Sep { get; set; }
        public double Oct { get; set; }
        public double Nov { get; set; }
        public double Dec { get; set; }
    }
}