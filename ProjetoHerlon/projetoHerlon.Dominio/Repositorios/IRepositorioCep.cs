﻿using Firebase.Auth;
using projetoHerlon.Dominio.ObjetoDominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace projetoHerlon.Dominio.Repositorios
{
    public interface IRepositorioCep
    {
        void ConverterListaParaJson(string EndereçoTexto, string EndereçoSalvarJson);
        List<ClasseCEP> ListaDeCeps(string EndereçoJson);
        ClasseCEP PesquisarCep(string EndereçoJson, string CEP);
    }
}
