﻿using projetoHerlon.Dominio.ObjetoDominio;
using System.Collections.Generic;


namespace projetoHerlon.Dominio.Repositorios
{
    public interface IRepositorioSolarimetrico
    {
        List<Solarimetrico> GetStruct(string endereço);
        Solarimetrico SearchCoord(List<Solarimetrico> classeStructures, double SearchLat, double SearchLong, int Precision = 2,int retries = 0);
        void CreateJson(string endereço, List<Solarimetrico> DataBase);
    }
}
