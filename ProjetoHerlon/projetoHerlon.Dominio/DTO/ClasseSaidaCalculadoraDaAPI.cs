﻿using projetoHerlon.Dominio.ObjetoDominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetoHerlon.Dominio.DTO
{
    public class ClasseSaidaCalculadoraDaAPI
    {
        public string Mensagem { get; set; }
        public double Radiação { get; set; }
        public double Celulas { get; set; }
        public int Potencia { get; set; }
        public double Eficiencia { get; set; }
    }
}