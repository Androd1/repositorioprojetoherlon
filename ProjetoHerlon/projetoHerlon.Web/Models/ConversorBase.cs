﻿using System;
using System.Collections.Generic;
using Excel = Microsoft.Office.Interop.Excel;
using System.Linq;
using System.Web;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Diagnostics;
using System.Threading;

namespace projetoHerlon.Web.Models
{
    public class ConversorBase
    {
        Thread thread;
        public void Novath()
        {
            thread = new Thread(new ThreadStart(this.init));
            thread.IsBackground = true;
            thread.Start();
        }
        private void init()
        {
            CreateJson(@"C:\Users\DELL\Desktop\TILTED_LATITUDE\tilted_latitude_means.json", GetStruct(@"C:\Users\DELL\Desktop\TILTED_LATITUDE\tilted_latitude_means.xlsx"));
        }
        public List<Struct> GetStruct(string endereço)
        {
            List<Struct> structs = new List<Struct>();

            Console.WriteLine("teste");

            Application xlApp = new Application();
            xlApp.Visible = true;
            Workbook xlWorkBook = xlApp.Workbooks.Open(endereço, 0, true, 5, "", "", true, XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            _Worksheet xlWorkSheet = (_Worksheet)xlWorkBook.Sheets[1];
            Range xlRange = xlWorkSheet.UsedRange;

            int rowCount = xlRange.Rows.Count;
            int colCount = xlRange.Columns.Count;

            Stopwatch stp = new Stopwatch();
            stp.Start();
            for (int i = 2; i < rowCount; i++)
            {

                Console.WriteLine(i.ToString());

                double _Id = (double)(xlWorkSheet.Cells[i, 1] as Range).Value;
                string _Country = (string)(xlWorkSheet.Cells[i, 2] as Range).Value;
                double _Lon = (double)(xlWorkSheet.Cells[i, 3] as Range).Value;
                double _Lat = (double)(xlWorkSheet.Cells[i, 4] as Range).Value;
                double _Annual = (double)(xlWorkSheet.Cells[i, 5] as Range).Value;
                double _Jan = (double)(xlWorkSheet.Cells[i, 6] as Range).Value;
                double _Feb = (double)(xlWorkSheet.Cells[i, 7] as Range).Value;
                double _Mar = (double)(xlWorkSheet.Cells[i, 8] as Range).Value;
                double _Apr = (double)(xlWorkSheet.Cells[i, 9] as Range).Value;
                double _May = (double)(xlWorkSheet.Cells[i, 10] as Range).Value;
                double _Jun = (double)(xlWorkSheet.Cells[i, 11] as Range).Value;
                double _Jul = (double)(xlWorkSheet.Cells[i, 12] as Range).Value;
                double _Aug = (double)(xlWorkSheet.Cells[i, 13] as Range).Value;
                double _Sep = (double)(xlWorkSheet.Cells[i, 14] as Range).Value;
                double _Oct = (double)(xlWorkSheet.Cells[i, 15] as Range).Value;
                double _Nov = (double)(xlWorkSheet.Cells[i, 16] as Range).Value;
                double _Dec = (double)(xlWorkSheet.Cells[i, 17] as Range).Value;



                structs.Add(new Struct
                {
                Id = _Id,
                Country = _Country,
                Lon = _Lon,
                Lat = _Lat,
                Annual = _Annual,
                Jan = _Jan,
                Feb = _Feb,
                Mar = _Mar,
                Apr = _Apr,
                May = _May,
                Jun = _Jun,
                Jul = _Jul,
                Aug = _Aug,
                Sep = _Sep,
                Oct = _Oct,
                Nov = _Nov,
                Dec = _Dec
                });
                
            }

            xlWorkBook.Close();
            xlApp.Quit();
            
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);
            stp.Stop();

            return structs;
        }
        public void CreateJson(string endereço, List<Struct> DataBase)
        {
            File.WriteAllText(endereço, JsonConvert.SerializeObject(DataBase));
        }
    }
    public class Struct
    {
        public double Id { get; set; }
        public string Country { get; set; }
        public double Lon { get; set; }
        public double Lat { get; set; }
        public double Annual { get; set; }
        public double Jan { get; set; }
        public double Feb { get; set; }
        public double Mar { get; set; }
        public double Apr { get; set; }
        public double May { get; set; }
        public double Jun { get; set; }
        public double Jul { get; set; }
        public double Aug { get; set; }
        public double Sep { get; set; }
        public double Oct { get; set; }
        public double Nov { get; set; }
        public double Dec { get; set; }
    }
}