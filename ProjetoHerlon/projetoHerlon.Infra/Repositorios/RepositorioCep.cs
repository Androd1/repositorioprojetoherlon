﻿using Newtonsoft.Json;
using projetoHerlon.Dominio.ObjetoDominio;
using projetoHerlon.Dominio.Repositorios;
using projetoHerlon.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace projetoHerlon.Infra.Repositorios
{
    public class RepositorioCep : IRepositorioCep
    {
        public void ConverterListaParaJson(string EndereçoTexto, string EndereçoSalvarJson)
        {
            List<ClasseCEP> _ClasseCEP = LerLista(EndereçoTexto);
            File.WriteAllText(EndereçoSalvarJson, JsonConvert.SerializeObject(_ClasseCEP));
        }
        public List<ClasseCEP> ListaDeCeps(string EndereçoJson)
        {
            var json = File.ReadAllText(EndereçoJson);
            var Lista = JsonConvert.DeserializeObject<List<ClasseCEP>>(json);
            return Lista;
        }
        public ClasseCEP PesquisarCep(string EndereçoJson,string CEP)
        {
            var json = File.ReadAllText(EndereçoJson);
            var Lista = JsonConvert.DeserializeObject<List<ClasseCEP>>(json);

            ClasseCEP classeCEP = new ClasseCEP();

            if (true)// mais rapido!!!
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var lookup2 = Lista.ToLookup(r => new { r.CEP, r.Rua, r.Bairro, r.Cidade, r.Estado });
                foreach (var group in lookup2)
                {
                    if (group.Key.CEP == CEP)
                    {
                        classeCEP.CEP = group.Key.CEP;
                        classeCEP.Rua = group.Key.Rua;
                        classeCEP.Bairro = group.Key.Bairro;
                        classeCEP.Cidade = group.Key.Cidade;
                        classeCEP.Estado = group.Key.Estado;
                        watch.Stop();
                        var elapsedMs = watch.ElapsedMilliseconds;
                        return classeCEP;
                    }
                }
            }
            else if(true) //dobro do tempo
            {
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var lookup = Lista.ToLookup(r => new { r.CEP, r.Rua, r.Bairro, r.Cidade, r.Estado });
                
                var key = new { classeCEP.CEP, classeCEP.Rua, classeCEP.Bairro, classeCEP.Cidade, classeCEP.Estado };

                var match = lookup[key].FirstOrDefault(r => r.CEP.StartsWith(CEP));
                classeCEP.CEP = match.CEP;
                classeCEP.Rua = match.Rua;
                classeCEP.Bairro = match.Bairro;
                classeCEP.Cidade = match.Cidade;
                classeCEP.Estado = match.Estado;
                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;
                return classeCEP;
            }




            return null;
        }
        private List<ClasseCEP> LerLista(string fileName)
        {
            List<ClasseCEP> _ClasseCEP = new List<ClasseCEP>();

            const Int32 BufferSize = 128;
            using (var fileStream = File.OpenRead(fileName))
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
            {
                String line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    var splited = line.Split('\t').ToList();
                    var CidadeEstado = splited[1].Split('/').ToList();

                    _ClasseCEP.Add(new ClasseCEP
                    {
                        CEP = splited[0],
                        Cidade = CidadeEstado[0],
                        Estado = CidadeEstado[1],
                        Bairro = splited[2],
                        Rua = splited[3]
                    });

                }
            }
            return _ClasseCEP;
        }
    }
}