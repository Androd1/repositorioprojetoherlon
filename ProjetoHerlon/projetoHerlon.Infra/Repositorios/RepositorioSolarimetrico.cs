﻿using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using projetoHerlon.Dominio.ObjetoDominio;
using projetoHerlon.Dominio.Repositorios;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;

namespace projetoHerlon.Web.Models
{
    public class RepositorioSolarimetrico : IRepositorioSolarimetrico
    {
        private Solarimetrico TempList;
        public List<Solarimetrico> GetStruct(string endereço)
        {
            List<Solarimetrico> structs = new List<Solarimetrico>();

            if (endereço.Contains("xls"))
            {


                Application xlApp = new Application
                {
                    Visible = Config.Debug
                };
                Workbook xlWorkBook = xlApp.Workbooks.Open(endereço, 0, true, 5, "", "", true, XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                _Worksheet xlWorkSheet = (_Worksheet)xlWorkBook.Sheets[1];
                Range xlRange = xlWorkSheet.UsedRange;

                Stopwatch stp = new Stopwatch();
                stp.Start();

                int rowCount = xlRange.Rows.Count;
                int colCount = xlRange.Columns.Count;

                Range range = xlWorkSheet.get_Range("A1", "Q72273");
                object[,] values = (object[,])range.Value2;
                int NumRow = 2;
                while (NumRow < values.GetLength(0))
                {
                    //for (int c = 1; c <= colCount; c++)
                    {
                        //var temp = Convert.ToString(values[NumRow, c]);

                        double _Id = Convert.ToDouble(values[NumRow, 1]);
                        string _Country = Convert.ToString(values[NumRow, 2]);
                        double _Lon = Convert.ToDouble(values[NumRow, 3]);
                        double _Lat = Convert.ToDouble(values[NumRow, 4]);
                        double _Annual = Convert.ToDouble(values[NumRow, 5]);
                        double _Jan = Convert.ToDouble(values[NumRow, 6]);
                        double _Feb = Convert.ToDouble(values[NumRow, 7]);
                        double _Mar = Convert.ToDouble(values[NumRow, 8]);
                        double _Apr = Convert.ToDouble(values[NumRow, 9]);
                        double _May = Convert.ToDouble(values[NumRow, 10]);
                        double _Jun = Convert.ToDouble(values[NumRow, 11]);
                        double _Jul = Convert.ToDouble(values[NumRow, 12]);
                        double _Aug = Convert.ToDouble(values[NumRow, 13]);
                        double _Sep = Convert.ToDouble(values[NumRow, 14]);
                        double _Oct = Convert.ToDouble(values[NumRow, 15]);
                        double _Nov = Convert.ToDouble(values[NumRow, 16]);
                        double _Dec = Convert.ToDouble(values[NumRow, 17]);

                        structs.Add(new Solarimetrico
                        {
                            Id = _Id,
                            Country = _Country,
                            Lon = _Lon,
                            Lat = _Lat,
                            Annual = _Annual,
                            Jan = _Jan,
                            Feb = _Feb,
                            Mar = _Mar,
                            Apr = _Apr,
                            May = _May,
                            Jun = _Jun,
                            Jul = _Jul,
                            Aug = _Aug,
                            Sep = _Sep,
                            Oct = _Oct,
                            Nov = _Nov,
                            Dec = _Dec
                        });
                    }
                    NumRow++;
                }

                xlWorkBook.Close();
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);
                stp.Stop();
            }
            else
            {
                WebClient client = new WebClient();
                Stream stream = client.OpenRead(endereço);
                StreamReader reader = new StreamReader(stream);
                String json = reader.ReadToEnd();
                structs = JsonConvert.DeserializeObject<List<Solarimetrico>>(json);
            }

            return structs;
        }
        public Solarimetrico SearchCoord(List<Solarimetrico> Solarimetricos, double SearchLat, double SearchLong, int Precision = 2, int retries = 0)
        {
            if (retries >= 10)
            {
                return null;
            }
            //ordenar lat
            List<Solarimetrico> SortedList = Solarimetricos.OrderBy(o => o.Lat).ToList();
            //procurar lat
            for (int i = 0; i < SortedList.Count; i++)
            {
                double temp = Math.Round(SortedList[i].Lat / 10000, 1);
                SearchLat = Math.Round(SearchLat, 1);

                if (temp == SearchLat)
                {
                    temp = Math.Round(SortedList[i].Lon / 10000, Precision);
                    SearchLong = Math.Round(SearchLong, Precision);
                    if (temp == SearchLong)
                    {
                        TempList = SortedList[i];
                        return TempList;
                    }
                }
            }
            SearchCoord(Solarimetricos, SearchLat, SearchLong - 0.01f, Precision, retries + 1);
            return TempList;
        }
        public void CreateJson(string endereço, List<Solarimetrico> DataBase)
        {
            File.WriteAllText(endereço, JsonConvert.SerializeObject(DataBase));
        }
    }
}