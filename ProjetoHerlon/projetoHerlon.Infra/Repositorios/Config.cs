﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Windows;

namespace projetoHerlon.Web.Models
{
    public static class Config
    {
        public static bool Debug = bool.Parse(WebConfigurationManager.AppSettings["DebugMode"]);

        public static void Message(string texto)
        {
            if (Debug)
            {
                MessageBox.Show(texto, "Debug", MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK, MessageBoxOptions.DefaultDesktopOnly);
            }
        }
    }
}