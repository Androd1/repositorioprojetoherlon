﻿using projetoHerlon.Dominio.DTO;
using projetoHerlon.Dominio.ObjetoDominio;
using projetoHerlon.Dominio.Repositorios;
using projetoHerlon.Dominio.Util;
using projetoHerlon.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Web.Http;

namespace projetoHerlon.Api.Controllers
{
    public class ApiCalculadoraController : ApiController
    {


        [HttpPost()]
        [Route("api/ApiCalculadora")]
        public ClasseSaidaCalculadoraDaAPI Placas([FromBody] ClasseObjetoCalculadora entrada)
        {
            IRepositorioSolarimetrico _conversorBase = IoC.Resolver<IRepositorioSolarimetrico>();

            List<Solarimetrico> listaparatestes = null;
           
            if (entrada.Excel)
            {
                try
                {
                    listaparatestes = _conversorBase.GetStruct(Directory.GetFiles(RepositorioArquivosExternos.ArquivosExternos, "*.xlsx", SearchOption.TopDirectoryOnly)[0]);
                }
                catch
                {
                    listaparatestes = _conversorBase.GetStruct(@"https://firebasestorage.googleapis.com/v0/b/fabulosas-16e68.appspot.com/o/tilted_latitude_means.xlsx?alt=media&token=967847f5-a34e-4aac-bdb5-0209cfc8cf38");
                }
                _conversorBase.CreateJson(Directory.GetFiles(RepositorioArquivosExternos.ArquivosExternos, "*.json", SearchOption.TopDirectoryOnly).ToString(), listaparatestes);
            }
            else
            {
                try
                {
                    listaparatestes = _conversorBase.GetStruct(Directory.GetFiles(RepositorioArquivosExternos.ArquivosExternos, "*.json", SearchOption.TopDirectoryOnly)[0]);
                }
                catch
                {
                    listaparatestes = _conversorBase.GetStruct(@"https://firebasestorage.googleapis.com/v0/b/fabulosas-16e68.appspot.com/o/tilted_latitude_means.json?alt=media&token=ad6737d2-5199-4852-b0bd-e3eaf8ef6591");
                }

            }
            return Calc(_conversorBase.SearchCoord(listaparatestes, entrada.Latitude, entrada.Longitude), entrada.Celula, entrada.Eficiencia, entrada.Consumo);
        }

        private ClasseSaidaCalculadoraDaAPI Calc(Solarimetrico Item, int Potencia,double Eficiencia, double Media)
        {
            ClasseSaidaCalculadoraDaAPI classeSaidaCalculadoraDaAPI = new ClasseSaidaCalculadoraDaAPI();
            
            if (Item != null)
            {
                var _const = Potencia * (Eficiencia/100) * (Item.Annual / 1000) * 30.4;
                double result = Math.Round(Media / (_const / 1000),1);

                classeSaidaCalculadoraDaAPI.Mensagem = $"{result} celulas de {Potencia}wp projetando uma eficiencia de {Eficiencia}%";
                if (result % Math.Truncate(result) > 0)
                {
                    result = Math.Round(Math.Truncate(result) + 1, 0);
                }
                classeSaidaCalculadoraDaAPI.Celulas = result;
                classeSaidaCalculadoraDaAPI.Potencia = Potencia;
                classeSaidaCalculadoraDaAPI.Eficiencia = Eficiencia;
                classeSaidaCalculadoraDaAPI.Radiação = Math.Round(Item.Annual / 1000,1);


                return classeSaidaCalculadoraDaAPI;
            }
            return null;
        }
    }
}
