﻿using Firebase.Auth;
using projetoHerlon.Dominio.DTO;
using projetoHerlon.Dominio.Repositorios;
using projetoHerlon.Dominio.Util;
using projetoHerlon.Web.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Web.Configuration;
using System.Web.Http;

namespace projetoHerlon.Api.Controllers
{
    public class ApiThreadController : ApiController
    {
        private IRepositorioFireBase _Firebase = IoC.Resolver<IRepositorioFireBase>();
        private readonly string FireBaseKey = WebConfigurationManager.AppSettings["FireBaseKey"];

        //localhost:51297/api/ApiThread
        [HttpGet]
        public Thread StartThread()
        {
            if (RepositorioArquivosExternos.AtualizarToken == null)
            {
                RepositorioArquivosExternos.AtualizarToken = new Thread(new ThreadStart(this.ThreadUpdateFB));
                RepositorioArquivosExternos.AtualizarToken.SetApartmentState(ApartmentState.STA);
                RepositorioArquivosExternos.AtualizarToken.IsBackground = true;
                RepositorioArquivosExternos.AtualizarToken.Name = "ThreadToken";
                RepositorioArquivosExternos.AtualizarToken.Start();
            }

            return RepositorioArquivosExternos.AtualizarToken;
        }
        private void ThreadUpdateFB()
        {
            int TempoAtualizarFB = 60;//segundos
            var AuthLink = _Firebase.Login(WebConfigurationManager.AppSettings["UserWrite"], WebConfigurationManager.AppSettings["PUserWrite"]);

            while (true)
            {
                _Firebase.RefreshAuth(AuthLink);

                if (AuthLink != null)
                {
                    var ResponseUpdate = _Firebase.UpdateDbChaveAleatoria(AuthLink.FirebaseToken, "RandomToken");
                }
                Thread.Sleep(TempoAtualizarFB * 1000);
            }
        }
    }
}
