﻿using projetoHerlon.Dominio.Repositorios;
using projetoHerlon.Dominio.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace projetoHerlon.Api.Controllers
{
    public class HomeController : Controller
    {
        private IRepositorioTeste2 _repositorioTeste = IoC.Resolver<IRepositorioTeste2>();

        public ActionResult Index()
        {           
            return View();
        }
    }
}
