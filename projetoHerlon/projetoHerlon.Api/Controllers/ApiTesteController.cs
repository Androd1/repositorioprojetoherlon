﻿using projetoHerlon.Dominio.DTO;
using projetoHerlon.Web.Models;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace projetoHerlon.Api.Controllers
{
    public class ApiTesteController : ApiController
    {
        #region POST
        //localhost:51297/api/ApiTeste
        [HttpPost]
        public ClasseRetornoDaAPITeste MetodoAPiTeste([FromBody] ClasseEntradaDaAPITeste entrada)
        {
            ClasseRetornoDaAPITeste retorno = new ClasseRetornoDaAPITeste();
            try
            {
                int IDID = entrada.ObjetoTeste.Id;
                string Name = entrada.ObjetoTeste.Nome;

                retorno.mensagem = "Funcionou, a pecinha acertou agora!!!Vlw André!!!";
                retorno.token = entrada.Token;
            }
            catch (Exception)
            {

                throw;
            }
            return retorno;

        }
        //localhost:51297/api/products/1
        [HttpPost()]
        [Route("api/products/{id:int}")]
        public string AddProduct([FromUri()] int id)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();

            return "Workou!!!!";
        }
        #endregion
        #region GET
        //localhost:51297/api/ApiTeste
        [HttpGet]
        public ClasseRetornoDaAPITeste ApiGET([FromBody] ClasseEntradaDaAPITeste entrada)
        {
            ClasseRetornoDaAPITeste classeRetornoDaAPITeste = null;
            try
            {
                //envio para o celular
                classeRetornoDaAPITeste = new ClasseRetornoDaAPITeste();
                classeRetornoDaAPITeste.token = RepositorioFireBase.RandomString(20);
                classeRetornoDaAPITeste.mensagem = "Enjoy!!!!";
            }
            catch (Exception)
            {
                throw;
            }
            return classeRetornoDaAPITeste;
        }
        #endregion
    }
}
