﻿using projetoHerlon.Dominio.ObjetoDominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetoHerlon.Dominio.DTO
{
    public class ClasseEntradaDaAPITeste
    {
        public ClasseObjetoTeste ObjetoTeste { get; set; }
        public string Token { get; set; }

    }
}