﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetoHerlon.Dominio.DTO
{
    public class ClasseRetornoDaAPITeste
    {
        public string token { get; set; }
        public string mensagem { get; set; }
    }
}