﻿using Firebase.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace projetoHerlon.Dominio.Repositorios
{
    public interface IRepositorioFireBase
    {
        bool CriarNovoUsuario(string User, string Password,string Name, [Optional] string Photo);
        FirebaseAuthLink Login(string User, string Password);
        void RefreshAuth(FirebaseAuthLink Auth);
        bool IsEmailVerified(FirebaseAuthLink Auth, FirebaseAuthProvider AuthProvider);
        string CriarChaveAleatoria(string FirebaseToken, string LocalId);
        string CriarNovaBaseUsuario(string FirebaseToken, string LocalId);
        string AssociarBaseUsuarioCliente(string FirebaseToken, string LocalId, string CpfCnpj, string FirebaseTokenClient, string LocalIdClient);
        string AdicionarNovoCliente(string FirebaseToken, string LocalId);
        string AdicionarNovoClienteCliente(string FirebaseToken, string LocalId, string _CpfCnpj);
        string AdicionarNovoConjugue(string FirebaseToken, string LocalId);
        string AdicionarNovoClienteConjugue(string FirebaseToken, string LocalId, string _CpfCnpj);
        string ReordenarClientes(string FirebaseToken, string LocalId);
        string PutFB(string FirebaseToken, string LocalId, string json);
        string GetFB(string FirebaseToken, string LocalId);
        string UpdateDbChaveAleatoria(string FirebaseToken, string LocalId);
    }
}
