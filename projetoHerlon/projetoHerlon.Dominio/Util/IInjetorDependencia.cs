﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projetoHerlon.Dominio.Util
{
    public interface IInjetorDependencia
    {
            T Resolver<T>();

            T Resolver<T>(Type type);
    }
}
