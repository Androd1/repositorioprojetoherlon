﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetoHerlon.Dominio.Util
{
    public class IoC
    {
        private static IInjetorDependencia _injetor;

        public static void Inicializar(IInjetorDependencia injetor)
        {
            _injetor = injetor;
        }

        public static T Resolver<T>()
        {
            return _injetor.Resolver<T>();
        }

        public static T Resolver<T>(Type type)
        {
            return _injetor.Resolver<T>(type);
        }
    }
}