﻿using projetoHerlon.Dominio.Repositorios;
using projetoHerlon.Dominio.Util;
using projetoHerlon.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace projetoHerlon.Web.Controllers
{
    public class AcessoController : Controller
    {
        private IRepositorioFireBase _Firebase = IoC.Resolver<IRepositorioFireBase>();

        [HttpGet]
        public ActionResult Logon()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Logon(ModelLogin login)
        {
            try
            {
                

                if (login.Senha != null && login.Usuario != null)
                {
                    var temp = _Firebase.Login(login.Usuario, login.Senha);
                    if (temp != null )
                    {
                        Session["Usuario"] = temp.User.DisplayName;
                        Session["Foto"] = temp.User.PhotoUrl;
                    }


                    return RedirectToAction("Index", "Home", new { id = "OK" });
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                throw ex.InnerException;
            }
           
        }
    }
}