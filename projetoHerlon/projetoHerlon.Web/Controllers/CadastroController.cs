﻿using projetoHerlon.Dominio.Repositorios;
using projetoHerlon.Dominio.Util;
using projetoHerlon.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projetoHerlon.Web.Controllers
{
    public class CadastroController : Controller
    {
        private IRepositorioFireBase _Firebase = IoC.Resolver<IRepositorioFireBase>();

        [HttpGet]
        public ActionResult CadastrarUsuario()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CadastrarUsuario(ModelCriarConta CriarConta)
        {
            try
            {
                if (CriarConta.Email != null && CriarConta.Senha != null && _Firebase.CriarNovoUsuario(CriarConta.Email, CriarConta.Senha,$"{CriarConta.Nome} {CriarConta.Sobrenome}"))
                {
                    var temp = _Firebase.Login(CriarConta.Email, CriarConta.Senha);
                    if (temp != null)
                    {
                        Session["Usuario"] = temp.User.DisplayName;
                        Session["Foto"] = temp.User.PhotoUrl;

                        return RedirectToAction("Index", "Home", new { id = "OK" });
                    }
                    return View();
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}