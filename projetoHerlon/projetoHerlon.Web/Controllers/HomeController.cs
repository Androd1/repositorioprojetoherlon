﻿using Newtonsoft.Json;
using projetoHerlon.Dominio.ObjetoDominio;
using projetoHerlon.Dominio.Repositorios;
using projetoHerlon.Dominio.Util;
using projetoHerlon.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projetoHerlon.Web.Controllers
{
    public class HomeController : Controller
    {
        private IRepositorioTeste2 _repositorioTeste = IoC.Resolver<IRepositorioTeste2>();

        private IRepositorioFireBase _FB = IoC.Resolver<IRepositorioFireBase>();
        private IRepositorioCep _CEP = IoC.Resolver<IRepositorioCep>();



        public ActionResult Index(string id)
        {

            if (Config.Debug)
            {
                //_CEP.ConverterListaParaJson(@"C:\Users\DELL\Desktop\ceps.txt", @"C:\Users\DELL\Desktop\ceps.json");
                //var lista = _CEP.ListaDeCeps(@"C:\Users\DELL\Desktop\ceps.json");
                var watch = System.Diagnostics.Stopwatch.StartNew();
                var lista = _CEP.PesquisarCep(@"C:\Users\DELL\Desktop\ceps.json", "32410330");
                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;



                //_FB.CriarNovoUsuario("asdasdtestea@hotmail.com", "meuzovo", "Herlon", "nulo");

                var authlink = _FB.Login("herlon_vk@hotmail.com", "meuzovo");

                var temp = _FB.GetFB(authlink.FirebaseToken, authlink.User.LocalId);
                if (temp == "null" || temp == "\"\"")
                {
                    Config.Message(_FB.CriarNovaBaseUsuario(authlink.FirebaseToken, authlink.User.LocalId));
                }

                _FB.AdicionarNovoConjugue(authlink.FirebaseToken, authlink.User.LocalId);


                var Reader = _FB.GetFB(authlink.FirebaseToken, authlink.User.LocalId);

                string json = "";

                if (Reader != "null")
                {
                    var jsonDs = JsonConvert.DeserializeObject<ClasseNovoUsuario>(Reader);
                    if (jsonDs != null && jsonDs._Clientes.Count < 5)
                    {
                        _FB.AdicionarNovoCliente(authlink.FirebaseToken, authlink.User.LocalId);
                        _FB.AdicionarNovoCliente(authlink.FirebaseToken, authlink.User.LocalId);
                    }
                }

                string conj = "nulo";
                _FB.AdicionarNovoClienteConjugue(authlink.FirebaseToken, authlink.User.LocalId, conj);
                _FB.AdicionarNovoClienteCliente(authlink.FirebaseToken, authlink.User.LocalId, conj);

                conj = "123";
                _FB.AdicionarNovoClienteConjugue(authlink.FirebaseToken, authlink.User.LocalId, conj);
                _FB.AdicionarNovoClienteCliente(authlink.FirebaseToken, authlink.User.LocalId, conj);

                _FB.ReordenarClientes(authlink.FirebaseToken, authlink.User.LocalId);

                var authlink2 = _FB.Login("cliente@vendedor.com.br", "Cliente que vende!!!");

                temp = _FB.GetFB(authlink.FirebaseToken, authlink.User.LocalId);
                if (temp == "null")
                {
                    Config.Message(_FB.CriarNovoUsuario("cliente@vendedor.com.br", "Cliente que vende!!!", "Sem Nome", "nulo2").ToString());
                }


                //EM TRABALHO!!!!
                //Config.Message(_FB.AssociarBaseUsuarioCliente(authlink.FirebaseToken, authlink.User.LocalId, "nulo", authlink2.FirebaseToken, authlink2.User.LocalId).ToString());
            }
            if (id == "OK")
            {
                return View();
            }
            else
            {
                return Redirect("~/Acesso/Logon");
            }

        }

        public ActionResult About(ModelTeste model)
        {
            var retorno = _repositorioTeste.MetodoTeste(model.NomeTeste);
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}