﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace projetoHerlon.Web.Models
{
    public class ModelLogin
    {
        public string Usuario { get; set; }
        public string Senha { get; set; }
    }
}