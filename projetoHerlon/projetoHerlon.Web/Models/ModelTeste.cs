﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace projetoHerlon.Web.Models
{
    public class ModelTeste
    {

        public ModelTeste()
        {
            NomeTeste = nameof(this.NomeTeste);
        }
        [Display(Name = "Label Teste")]
        public string NomeTeste { get; set; }
    }
}