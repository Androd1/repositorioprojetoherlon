﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace projetoHerlon.Web.Helpers
{
    public static class Bootstrap
    {
        #region BreadCrumb
        public static MvcHtmlString BreadCrumb(this HtmlHelper htmlHelper, Dictionary<string, string> links)
        {
            IList<KeyValuePair<string, string>> breadCrumbs = links.ToList();

            string html = string.Empty;
            html += "<ol class=\"breadcrumb\">";
            for (int i = 0; i < breadCrumbs.Count; ++i)
            {
                if (i < breadCrumbs.Count - 1)
                    html += "<li><a href=\"" + VirtualPathUtility.ToAbsolute(breadCrumbs[i].Value) + "\">" + breadCrumbs[i].Key + "</a></li>";
                else
                    html += "<li class=\"active\">" + breadCrumbs[i].Key + "</li>";
            }
            html += "</ol>";

            return MvcHtmlString.Create(html);
        }
        #endregion
    }
}