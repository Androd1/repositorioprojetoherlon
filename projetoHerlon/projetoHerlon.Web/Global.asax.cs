﻿using projetoHerlon.Dominio.Util;
using projetoHerlon.Infra.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace projetoHerlon.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public void Application_Start()
        {
            IoC.Inicializar(InjetorDependencia.Instancia());

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
