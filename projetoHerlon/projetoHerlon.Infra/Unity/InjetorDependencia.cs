﻿using projetoHerlon.Dominio.Repositorios;
using projetoHerlon.Dominio.Util;
using projetoHerlon.Infra.Repositorios;
using projetoHerlon.Web.Models;
using System;
using Unity;

namespace projetoHerlon.Infra.Unity
{
    public class InjetorDependencia : IInjetorDependencia
    {

        private readonly IUnityContainer _container;
        private static InjetorDependencia _injetorDependencia;


        private InjetorDependencia()
        {
            _container = new UnityContainer();
            MapearDependencias();
        }

        public static InjetorDependencia Instancia()
        {
            return _injetorDependencia ?? (_injetorDependencia = new InjetorDependencia());
        }


        public T Resolver<T>()
        {
            return _container.Resolve<T>();

        }

        public T Resolver<T>(Type type)
        {
            return (T)_container.Resolve(type);
        }

        private void MapearDependencias()
        {
            _container
                .RegisterType<IRepositorioTeste2, RepositorioTeste>() 
                .RegisterType<IRepositorioFireBase, RepositorioFireBase>()
                .RegisterType<IRepositorioSolarimetrico, RepositorioSolarimetrico>()
                .RegisterType<IRepositorioCep, RepositorioCep>();
        }
    }
}