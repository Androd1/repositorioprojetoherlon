﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace projetoHerlon.Infra.Criptografia
{
    public class RepositorioCriptografia
    {

        public string GerarCriptografia(string valor, string chave)
        {
            try
            {
                string _chave;
                _chave = GerarHashMd5(chave);

                byte[] byteChave = Encoding.UTF8.GetBytes(_chave);
                _chave = Convert.ToBase64String(byteChave);

                byte[] byteValor = Encoding.UTF8.GetBytes(valor);
                valor = Convert.ToBase64String(byteValor);

                Random rnd = new Random();
                int tam = valor.Length;
                int dtam = 50 - tam;

                for (int i = 0; i < dtam; i++)
                {
                    valor += i == 0 ? " " : rnd.Next(0, 9).ToString();
                }

                tam = _chave.Length;
                dtam = 50 - tam;
                for (int i = 0; i < dtam; i++)
                {
                    _chave += i == 0 ? " " : rnd.Next(0, 9).ToString();
                }

                string token = "";
                for (int i = 0; i < 50; i++)
                {
                    token += valor.Substring(i, 1) + _chave.Substring(i, 1);
                }

                byte[] byteToken = Encoding.UTF8.GetBytes(token);
                token = Convert.ToBase64String(byteToken);

                return token;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public string GerarHashMd5(string entrada)
        {
            MD5 md5Hash = MD5.Create();
            // Converter a String para array de bytes, que é como a biblioteca trabalha.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(entrada));
            // Cria-se um StringBuilder para recompôr a string.
            StringBuilder sBuilder = new StringBuilder();
            // Loop para formatar cada byte como uma String em hexadecimal
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}