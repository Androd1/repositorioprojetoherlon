﻿using Firebase.Auth;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using projetoHerlon.Dominio.ObjetoDominio;
using projetoHerlon.Dominio.Repositorios;
using System;
using System.Collections.Generic;

using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Web.Configuration;

namespace projetoHerlon.Web.Models
{
    public class RepositorioFireBase : IRepositorioFireBase
    {
        readonly string DataBase = WebConfigurationManager.AppSettings["FireBaseDataBase"];
        private readonly string FireBaseKey = WebConfigurationManager.AppSettings["FireBaseKey"];

        /// <summary>
        /// Documentação de referencia usando UNIT(que mais assemelha a este projeto)
        ///  https://firebase.google.com/docs/auth/unity/password-auth?authuser=0
        /// </summary>

        private static Random random = new Random();

        public bool CriarNovoUsuario(string User, string Password, string Name, [Optional] string Photo)
        {

            FirebaseAuthLink Auth;
            try
            {
                try
                {
                    Auth = Login(User, Password);
                    if (Auth == null)
                    {
                        throw new Exception();
                    }
                    return true;
                }
                catch
                {
                    string Message = "";
                    var authProvider = new FirebaseAuthProvider(new FirebaseConfig(FireBaseKey));
                    authProvider.CreateUserWithEmailAndPasswordAsync(User, Password, Name, true).ContinueWith(task =>
                    {
                        if (task.IsCanceled)
                        {
                            Message = "CreateUserWithEmailAndPasswordAsync was canceled.";
                            return;
                        }
                        if (task.IsFaulted)
                        {
                            Message = "CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception;
                            return;
                        }

                        // Firebase user has been created.
                        Auth = task.Result;
                        Message = $"Firebase user created successfully: {Auth.User.DisplayName} ({Auth.User.LocalId})";
                    });

                    do
                    {
                        //nothing
                    } while (Message == "");

                    if (authProvider != null && Message.Contains("successfully"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception(ex.InnerException.InnerException.Message);
            }

        }
        public FirebaseAuthLink Login(string User, string Password)
        {
            string Message = "";
            FirebaseAuthLink auth = null;
            var authProvider = new FirebaseAuthProvider(new FirebaseConfig(FireBaseKey));
            authProvider.SignInWithEmailAndPasswordAsync(User, Password).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Message = "SignInWithEmailAndPasswordAsync was canceled.";
                    return;
                }
                if (task.IsFaulted)
                {
                    Message = "SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception;
                    return;
                }

                auth = task.Result;
                Message = $"User signed in successfully: {auth.User.DisplayName} ({auth.User.LocalId})";
            });


            do
            {
                //nothing
            } while (Message == "");

            if (auth != null && IsEmailVerified(auth, authProvider))
            {
                return auth;
            }

            return null;
        }
        /*
        public void ConfigConta(int tipo,string email,[Optional] FirebaseAuthLink auth,[Optional] string NovaSenha, [Optional] string Nome,[Optional] string FotoUrl)
        {

            FirebaseConfig firebaseConfig = new FirebaseConfig(FireBaseKey);

            FirebaseAuthProvider temp = new FirebaseAuthProvider(firebaseConfig);

            switch (tipo)
            {
                case 0:
                    temp.SendPasswordResetEmailAsync(email);
                    break;
                case 1:
                    temp.ChangeUserPassword(auth.FirebaseToken, NovaSenha);
                    break;
                case 2:
                    temp.DeleteUser(auth.FirebaseToken);
                    break;
                case 3:
                    temp.SendEmailVerificationAsync(auth);
                    break;
                case 4:
                    temp.UpdateProfileAsync(auth.FirebaseToken,Nome, FotoUrl);
                    break;
                case 5:
                    //temp.SignInWithOAuthTwitterTokenAsync()
                    break;
                default:
                    break;
            }

        }*/
        public void RefreshAuth(FirebaseAuthLink Auth)
        {
            var authProvider = new FirebaseAuthProvider(new FirebaseConfig(FireBaseKey));
            authProvider.RefreshAuthAsync(Auth);

        }
        public bool IsEmailVerified(FirebaseAuthLink Auth, FirebaseAuthProvider AuthProvider)
        {
            return true;

            if (!Auth.User.IsEmailVerified)
            {
                var result = AuthProvider.SendEmailVerificationAsync(Auth).Status;
                return false;
            }
            else
            {
                return true;
            }
        }
        public string CriarChaveAleatoria(string FirebaseToken, string LocalId)
        {
            try
            {
                ChaveAleatoria _ChaveAleatoria = new ChaveAleatoria() { _ChaveAleatoria = "" };

                var json = JsonConvert.SerializeObject(_ChaveAleatoria);
                var request = WebRequest.CreateHttp($"{DataBase}");

                return PutFB(FirebaseToken, LocalId, json);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        public string CriarNovaBaseUsuario(string FirebaseToken, string LocalId)
        {
            try
            {
                ClasseNovoUsuario novoUsuario = new ClasseNovoUsuario()
                {
                    _DadosPessoais = new DadosPessoais()
                    {
                        Nome = "",
                        CpfCnpj = "",
                        RG = "",
                        OrgãoEmissor = "",
                        FiliaçãoMãe = "",
                        FiliaçãoPai = "",
                        Sexo = "",
                        Nascimento = ""
                    },
                    _EstadoCivil = "",
                    _Conjugue = new Conjugue()
                    {
                        DadosPessoaisConjugue = new DadosPessoais()
                    },
                    _DadosSistema = new DadosSistema()
                    {
                        NivelAcesso = 9,
                        TokenTemp = "",
                        Observações = "",
                        AreaDeMensagens = ""
                    },
                    _DadosCadastro = new DadosCadastro()
                    {
                        DataDeCadastro = DateTime.Now,
                        QuemCadastrou = "",
                        Região = ""
                    },
                    _Endereço = new Endereço()
                    {
                        Rua = "",
                        Numero = 0,
                        Complemento = "",
                        Bairro = "",
                        Cidade = "",
                        Estado = "",
                        Pais = "",
                        CEP = 0
                    },
                    _Contatos = new Contatos()
                    {
                        Celular1 = "",
                        Celular2 = "",
                        Residencial = "",
                        Comercial = "",
                        Email = "",
                        Site = ""
                    },
                    _DadosVendedor = new DadosVendedor()
                    {
                        Status = "",
                        DescontoMaximoAplicavel = 20f,
                        VendasConcretizadas = new List<Vendas>()
                    {
                        new Vendas()
                        {
                            Ano = DateTime.Now.Year,
                            Jan = 0f,
                            Fev = 0f,
                            Mar = 0f,
                            Abr = 0f,
                            Mai = 0f,
                            Jun = 0f,
                            Jul = 0f,
                            Ago = 0f,
                            Set = 0f,
                            Out = 0f,
                            Nov = 0f,
                            Dez = 0f,
                        }
                    },
                        VendasRealizadas = new List<Vendas>()
                    {
                        new Vendas()
                        {
                            Ano = DateTime.Now.Year,
                            Jan = 0f,
                            Fev = 0f,
                            Mar = 0f,
                            Abr = 0f,
                            Mai = 0f,
                            Jun = 0f,
                            Jul = 0f,
                            Ago = 0f,
                            Set = 0f,
                            Out = 0f,
                            Nov = 0f,
                            Dez = 0f,
                        }
                    }
                    },
                    _DadosConsumo = new DadosConsumo()
                    {
                        Latitude = 0f,
                        Longitude = 0f,
                        ConsumoMedio = 0
                    },
                    _Clientes = new List<Clientes>()
                    {
                        new Clientes()
                    }
                };

                var json = JsonConvert.SerializeObject(novoUsuario);
                var request = WebRequest.CreateHttp($"{DataBase}");

                return PutFB(FirebaseToken, LocalId, json);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        public string AssociarBaseUsuarioCliente(string FirebaseToken, string LocalId, string CpfCnpj, string FirebaseTokenClient, string LocalIdClient)
        {
            var Reader = GetFB(FirebaseToken);

            string json = "";
            if (Reader != "null")
            {

                //somente o vendedor podera cadastrar o cliente como vendedor
                //associar o cpf ao ID dele
                //resolver a questão de só conseguir logar o cliente baseado no login do vendedor, por isso mando dois auth...

                json = JsonConvert.SerializeObject(json);
            }

            return PutFB(FirebaseToken, LocalId, json);
        }
        public string AdicionarNovoCliente(string FirebaseToken, string LocalId)
        {
            var Reader = GetFB(FirebaseToken, LocalId);

            string json = "";
            if (Reader != "null")
            {
                var jsonDs = JsonConvert.DeserializeObject<ClasseNovoUsuario>(Reader);

                if (jsonDs._Clientes == null)
                {
                    jsonDs._Clientes = new List<Clientes>()
                    {
                        new Clientes()
                        {
                            Cliente = new ClasseNovoUsuario()
                    {
                        _DadosPessoais = new DadosPessoais()
                        {
                            Nome = "",
                            CpfCnpj = "",
                            RG = "",
                            OrgãoEmissor = "",
                            FiliaçãoMãe = "",
                            FiliaçãoPai = "",
                            Sexo = "",
                            Nascimento = ""
                        },
                        _EstadoCivil = "",
                        _DadosSistema = new DadosSistema()
                        {
                            NivelAcesso = 0,
                            TokenTemp = "",
                            Observações = "",
                            AreaDeMensagens = ""
                        },
                        _DadosCadastro = new DadosCadastro()
                        {
                            DataDeCadastro = DateTime.Now,
                            QuemCadastrou = LocalId,
                            Região = ""
                        },
                        _Endereço = new Endereço()
                        {
                            Rua = "",
                            Numero = 0,
                            Complemento = "",
                            Bairro = "",
                            Cidade = "",
                            Estado = "",
                            Pais = "",
                            CEP = 0
                        },
                        _Contatos = new Contatos()
                        {
                            Celular1 = "",
                            Celular2 = "",
                            Residencial = "",
                            Comercial = "",
                            Email = "",
                            Site = ""
                        },
                        _DadosVendedor = new DadosVendedor()
                        {
                            Status = "",
                            DescontoMaximoAplicavel = 0f,
                            VendasConcretizadas = new List<Vendas>()
                    {
                        new Vendas()
                        {
                            Ano = 2020,
                            Jan = 0f,
                            Fev = 0f,
                            Mar = 0f,
                            Abr = 0f,
                            Mai = 0f,
                            Jun = 0f,
                            Jul = 0f,
                            Ago = 0f,
                            Set = 0f,
                            Out = 0f,
                            Nov = 0f,
                            Dez = 0f,
                        }
                    },
                            VendasRealizadas = new List<Vendas>()
                    {
                        new Vendas()
                        {
                            Ano = 2020,
                            Jan = 0f,
                            Fev = 0f,
                            Mar = 0f,
                            Abr = 0f,
                            Mai = 0f,
                            Jun = 0f,
                            Jul = 0f,
                            Ago = 0f,
                            Set = 0f,
                            Out = 0f,
                            Nov = 0f,
                            Dez = 0f,
                        }
                    }
                        },
                        _DadosConsumo = new DadosConsumo()
                        {
                            Latitude = 0f,
                            Longitude = 0f,
                            ConsumoMedio = 0
                        }
                    }
                        }
                    };
                }
                else
                {
                    jsonDs._Clientes.Add(new Clientes()
                    {
                        Cliente = new ClasseNovoUsuario()
                        {
                            _DadosPessoais = new DadosPessoais()
                            {
                                Nome = "",
                                CpfCnpj = "",
                                RG = "",
                                OrgãoEmissor = "",
                                FiliaçãoMãe = "",
                                FiliaçãoPai = "",
                                Sexo = "",
                                Nascimento = ""
                            },
                            _EstadoCivil = "",
                            _Conjugue = new Conjugue(),
                            _DadosSistema = new DadosSistema()
                            {
                                NivelAcesso = 0,
                                TokenTemp = "",
                                Observações = "",
                                AreaDeMensagens = ""
                            },
                            _DadosCadastro = new DadosCadastro()
                            {
                                DataDeCadastro = DateTime.Now,
                                QuemCadastrou = LocalId,
                                Região = ""
                            },
                            _Endereço = new Endereço()
                            {
                                Rua = "",
                                Numero = 0,
                                Complemento = "",
                                Bairro = "",
                                Cidade = "",
                                Estado = "",
                                Pais = "",
                                CEP = 0
                            },
                            _Contatos = new Contatos()
                            {
                                Celular1 = "",
                                Celular2 = "",
                                Residencial = "",
                                Comercial = "",
                                Email = "",
                                Site = ""
                            },
                            _DadosVendedor = new DadosVendedor()
                            {
                                Status = "",
                                DescontoMaximoAplicavel = 20f,
                                VendasConcretizadas = new List<Vendas>()
                    {
                        new Vendas()
                        {
                            Ano = 2020,
                            Jan = 0f,
                            Fev = 0f,
                            Mar = 0f,
                            Abr = 0f,
                            Mai = 0f,
                            Jun = 0f,
                            Jul = 0f,
                            Ago = 0f,
                            Set = 0f,
                            Out = 0f,
                            Nov = 0f,
                            Dez = 0f,
                        }
                    },
                                VendasRealizadas = new List<Vendas>()
                    {
                        new Vendas()
                        {
                            Ano = 2020,
                            Jan = 0f,
                            Fev = 0f,
                            Mar = 0f,
                            Abr = 0f,
                            Mai = 0f,
                            Jun = 0f,
                            Jul = 0f,
                            Ago = 0f,
                            Set = 0f,
                            Out = 0f,
                            Nov = 0f,
                            Dez = 0f,
                        }
                    }
                            },
                            _DadosConsumo = new DadosConsumo()
                            {
                                Latitude = 0f,
                                Longitude = 0f,
                                ConsumoMedio = 0
                            }
                        }
                    });
                }
                json = JsonConvert.SerializeObject(jsonDs);
            }

            return PutFB(FirebaseToken, LocalId, json);
        }
        public string AdicionarNovoClienteCliente(string FirebaseToken, string LocalId, string _CpfCnpj)
        {
            var Reader = GetFB(FirebaseToken, LocalId);

            string json = "";
            if (Reader != "null")
            {
                var jsonDs = JsonConvert.DeserializeObject<ClasseNovoUsuario>(Reader);
                if (jsonDs._Clientes.Count > 0)
                {
                    for (int i = 0, n = jsonDs._Clientes.Count; i < n; i++)
                    {
                        if (jsonDs._Clientes[i] != null && jsonDs._Clientes[i].Cliente._DadosPessoais.CpfCnpj == _CpfCnpj)
                        {
                            if (jsonDs._Clientes[i].Cliente._Clientes == null)
                            {
                                jsonDs._Clientes[i].Cliente._Clientes = new List<Clientes>()
                                {
                                    new Clientes()
                                    {
                                        Cliente = new ClasseNovoUsuario()
                                        {
                                            _DadosPessoais = new DadosPessoais()
                                        {
                                            Nome = "",
                                            CpfCnpj = "",
                                            RG = "",
                                            OrgãoEmissor = "",
                                            FiliaçãoMãe = "",
                                            FiliaçãoPai = "",
                                            Sexo = "",
                                            Nascimento = ""
                                        },
                                            _EstadoCivil = "",
                                            _Conjugue = new Conjugue(),
                                            _DadosSistema = new DadosSistema()
                                        {
                                            NivelAcesso = 0,
                                            TokenTemp = "",
                                            Observações = "",
                                            AreaDeMensagens = ""
                                        },
                                            _DadosCadastro = new DadosCadastro()
                                        {
                                            DataDeCadastro = DateTime.Now,
                                            QuemCadastrou = LocalId,
                                            Região = ""
                                        },
                                            _Endereço = new Endereço()
                                        {
                                            Rua = "",
                                            Numero = 0,
                                            Complemento = "",
                                            Bairro = "",
                                            Cidade = "",
                                            Estado = "",
                                            Pais = "",
                                            CEP = 0
                                        },
                                            _Contatos = new Contatos()
                                        {
                                            Celular1 = "",
                                            Celular2 = "",
                                            Residencial = "",
                                            Comercial = "",
                                            Email = "",
                                            Site = ""
                                        },
                                            _DadosVendedor = new DadosVendedor()
                                        {
                                            Status = "",
                                            DescontoMaximoAplicavel = 20f,
                                            VendasConcretizadas = new List<Vendas>()
                    {
                        new Vendas()
                        {
                            Ano = 2020,
                            Jan = 0f,
                            Fev = 0f,
                            Mar = 0f,
                            Abr = 0f,
                            Mai = 0f,
                            Jun = 0f,
                            Jul = 0f,
                            Ago = 0f,
                            Set = 0f,
                            Out = 0f,
                            Nov = 0f,
                            Dez = 0f,
                        }
                    },
                                            VendasRealizadas = new List<Vendas>()
                    {
                        new Vendas()
                        {
                            Ano = 2020,
                            Jan = 0f,
                            Fev = 0f,
                            Mar = 0f,
                            Abr = 0f,
                            Mai = 0f,
                            Jun = 0f,
                            Jul = 0f,
                            Ago = 0f,
                            Set = 0f,
                            Out = 0f,
                            Nov = 0f,
                            Dez = 0f,
                        }
                    }
                                        },
                                            _DadosConsumo = new DadosConsumo()
                                        {
                                            Latitude = 0f,
                                            Longitude = 0f,
                                            ConsumoMedio = 0
                                        }
                                        }
                                    }
                                };
                                break;

                            }
                            else
                            {
                                jsonDs._Clientes[i].Cliente._Clientes.Add(new Clientes()
                                {
                                    Cliente = new ClasseNovoUsuario()
                                    {
                                        _DadosPessoais = new DadosPessoais()
                                        {
                                            Nome = "",
                                            CpfCnpj = "",
                                            RG = "",
                                            OrgãoEmissor = "",
                                            FiliaçãoMãe = "",
                                            FiliaçãoPai = "",
                                            Sexo = "",
                                            Nascimento = ""
                                        },
                                        _EstadoCivil = "",
                                        _Conjugue = new Conjugue(),
                                        _DadosSistema = new DadosSistema()
                                        {
                                            NivelAcesso = 0,
                                            TokenTemp = "",
                                            Observações = "",
                                            AreaDeMensagens = ""
                                        },
                                        _DadosCadastro = new DadosCadastro()
                                        {
                                            DataDeCadastro = DateTime.Now,
                                            QuemCadastrou = LocalId,
                                            Região = ""
                                        },
                                        _Endereço = new Endereço()
                                        {
                                            Rua = "",
                                            Numero = 0,
                                            Complemento = "",
                                            Bairro = "",
                                            Cidade = "",
                                            Estado = "",
                                            Pais = "",
                                            CEP = 0
                                        },
                                        _Contatos = new Contatos()
                                        {
                                            Celular1 = "",
                                            Celular2 = "",
                                            Residencial = "",
                                            Comercial = "",
                                            Email = "",
                                            Site = ""
                                        },
                                        _DadosVendedor = new DadosVendedor()
                                        {
                                            Status = "",
                                            DescontoMaximoAplicavel = 20f,
                                            VendasConcretizadas = new List<Vendas>()
                    {
                        new Vendas()
                        {
                            Ano = 2020,
                            Jan = 0f,
                            Fev = 0f,
                            Mar = 0f,
                            Abr = 0f,
                            Mai = 0f,
                            Jun = 0f,
                            Jul = 0f,
                            Ago = 0f,
                            Set = 0f,
                            Out = 0f,
                            Nov = 0f,
                            Dez = 0f,
                        }
                    },
                                            VendasRealizadas = new List<Vendas>()
                    {
                        new Vendas()
                        {
                            Ano = 2020,
                            Jan = 0f,
                            Fev = 0f,
                            Mar = 0f,
                            Abr = 0f,
                            Mai = 0f,
                            Jun = 0f,
                            Jul = 0f,
                            Ago = 0f,
                            Set = 0f,
                            Out = 0f,
                            Nov = 0f,
                            Dez = 0f,
                        }
                    }
                                        },
                                        _DadosConsumo = new DadosConsumo()
                                        {
                                            Latitude = 0f,
                                            Longitude = 0f,
                                            ConsumoMedio = 0
                                        }
                                    }
                                });
                                break;
                            }
                        }
                    }
                    json = JsonConvert.SerializeObject(jsonDs);
                }
            }

            return PutFB(FirebaseToken, LocalId, json);
        }
        public string AdicionarNovoConjugue(string FirebaseToken, string LocalId)
        {
            var Reader = GetFB(FirebaseToken, LocalId);

            string json = "";
            if (Reader != "null")
            {
                var jsonDs = JsonConvert.DeserializeObject<ClasseNovoUsuario>(Reader);

                if (jsonDs._Conjugue == null)
                {
                    jsonDs._Conjugue = new Conjugue()
                    {
                        DadosPessoaisConjugue = new DadosPessoais()
                        {
                            Nome = "",
                            CpfCnpj = "",
                            RG = "",
                            OrgãoEmissor = "",
                            FiliaçãoMãe = "",
                            FiliaçãoPai = "",
                            Sexo = "",
                            Nascimento = ""
                        }
                    };
                }
                else
                {
                    jsonDs._Conjugue.DadosPessoaisConjugue = new DadosPessoais()
                    {
                        Nome = "",
                        CpfCnpj = "",
                        RG = "",
                        OrgãoEmissor = "",
                        FiliaçãoMãe = "",
                        FiliaçãoPai = "",
                        Sexo = "",
                        Nascimento = ""
                    };
                }
                json = JsonConvert.SerializeObject(jsonDs);
            }

            return PutFB(FirebaseToken, LocalId, json);
        }
        public string AdicionarNovoClienteConjugue(string FirebaseToken, string LocalId, string _CpfCnpj)
        {
            var Reader = GetFB(FirebaseToken, LocalId);

            string json = "";
            if (Reader != "null")
            {
                var jsonDs = JsonConvert.DeserializeObject<ClasseNovoUsuario>(Reader);
                if (jsonDs._Clientes.Count > 0)
                {
                    for (int i = 0, n = jsonDs._Clientes.Count; i < n; i++)
                    {
                        if (jsonDs._Clientes[i] != null && jsonDs._Clientes[i].Cliente._DadosPessoais.CpfCnpj == _CpfCnpj)
                        {
                            jsonDs._Clientes[i].Cliente._Conjugue = new Conjugue()
                            {
                                DadosPessoaisConjugue = new DadosPessoais()
                                {
                                    Nome = "",
                                    CpfCnpj = "",
                                    RG = "",
                                    OrgãoEmissor = "",
                                    FiliaçãoMãe = "",
                                    FiliaçãoPai = "",
                                    Sexo = "",
                                    Nascimento = ""
                                }
                            };
                            break;
                        }
                    }
                }

                json = JsonConvert.SerializeObject(jsonDs);
            }

            return PutFB(FirebaseToken, LocalId, json);
        }
        public string ReordenarClientes(string FirebaseToken, string LocalId)
        {
            var Reader = GetFB(FirebaseToken, LocalId);

            string json = "";
            if (Reader != "null")
            {
                var jsonDs = JsonConvert.DeserializeObject<ClasseNovoUsuario>(Reader);

                if (jsonDs._Clientes.Count > 0)
                {
                    for (int i = 0; i < jsonDs._Clientes.Count; i++)
                    {
                        if (jsonDs._Clientes[i] == null)
                        {
                            jsonDs._Clientes.Remove(jsonDs._Clientes[i]);
                        }
                    }
                }
                json = JsonConvert.SerializeObject(jsonDs);
            }

            return PutFB(FirebaseToken, LocalId, json);
        }
        public string PutFB(string FirebaseToken, string LocalId, string json)
        {
            try
            {
                if (json == "")
                {
                    return "Erro";
                }
                else
                {


                    var request = WebRequest.CreateHttp($"{DataBase}");

                    if (FirebaseToken == "")
                    {
                        if (LocalId != "")
                            request = WebRequest.CreateHttp($"{DataBase}{LocalId}.json");
                        else
                            request = WebRequest.CreateHttp($"{DataBase}.json");
                    }
                    else
                    {
                        if (LocalId != "")
                            request = WebRequest.CreateHttp($"{DataBase}{LocalId}.json?auth={FirebaseToken}");
                        else
                            request = WebRequest.CreateHttp($"{DataBase}.json?auth={FirebaseToken}");
                    }


                    request.Method = "PUT";
                    request.ContentType = "application/json";
                    var buffer = Encoding.UTF8.GetBytes(json);
                    request.ContentLength = buffer.Length;
                    request.GetRequestStream().Write(buffer, 0, buffer.Length);

                    return "sucess";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }
        public string GetFB(string FirebaseToken, string LocalId = "")
        {
            try
            {
                var request = WebRequest.CreateHttp($"{DataBase}.json");
                if (LocalId == "")
                    request = WebRequest.CreateHttp($"{DataBase}.json?auth={FirebaseToken}");
                else
                    request = WebRequest.CreateHttp($"{DataBase}{LocalId}.json?auth={FirebaseToken}");
                var Response = (HttpWebResponse)request.GetResponse();
                var Reader = new StreamReader(Response.GetResponseStream()).ReadToEnd();

                return Reader;
            }
            catch (Exception ex)
            {

                return ex.Message;
            }

        }
        public string UpdateDbChaveAleatoria(string FirebaseToken, string LocalId)
        {
            try
            {
                var Reader = GetFB(FirebaseToken, LocalId);

                string json = "";

                if (Reader != "null")
                {
                    var jsonDs = JsonConvert.DeserializeObject<ChaveAleatoria>(Reader);
                    jsonDs._ChaveAleatoria = RandomString(20);
                    json = JsonConvert.SerializeObject(jsonDs);
                }
                else
                {
                    CriarChaveAleatoria(FirebaseToken, LocalId);
                }

                return PutFB(FirebaseToken, LocalId, json);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}